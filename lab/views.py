from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import Schedules
from .models import Schedule

# Create your views here.

response = {}


def index(request):
    response['navItem'] = 'index'
    return render(request, 'index.html', response)


def cv(request):
    response['navItem'] = 'cv'
    return render(request, 'cv.html', response)


def portfolio(request):
    response['navItem'] = 'portfolio'
    return render(request, 'portfolio.html', response)


def about(request):
    response['navItem'] = 'about'
    return render(request, 'about.html', response)


def contact(request):
    response['navItem'] = 'contact'
    return render(request, 'contact.html', response)


def schedule(request):
    return HttpResponseRedirect('/post_schedule')


def post_schedule(request):
    if request.method == 'POST':
        form = Schedules(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Schedule.objects.create(**data)
            return HttpResponseRedirect('/all_schedules')
    else:
        form = Schedules()
    response['navItem'] = 'schedule'
    response['post_schedule'] = Schedules
    response['form'] = form
    return render(request, 'formschedule.html', response)


def all_schedules(request):
    response['navItem'] = 'all_schedules'
    response['activities'] = Schedule.objects.all()
    return render(request, 'all_schedules.html', response)

def deleteall(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect('all_schedules')