from django.conf.urls import url
from .views import index, cv, portfolio, about, contact, schedule, post_schedule, all_schedules, deleteall

urlpatterns = [
    url('index', index),
    url('cv', cv),
    url('portfolio', portfolio),
    url('about', about),
    url('contact', contact),
    url('deleteall', deleteall, name='deleteall'),
    url('post_schedule', post_schedule, name='post_schedule'),
    url('all_schedules', all_schedules, name='all_schedules'),
    url('schedule', schedule, name='schedule'),
    url('', index)
]