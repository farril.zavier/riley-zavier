from django import forms

class Schedules(forms.Form):
    activity = forms.CharField(label='Activity', required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}))
    time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}))
    place = forms.CharField(label='Place', required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
    category = forms.CharField(label='Category', required=True, max_length=50, widget=forms.TextInput(attrs={'class': 'form-control'}))
