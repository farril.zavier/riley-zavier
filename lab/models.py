from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Schedule(models.Model):
    activity = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
